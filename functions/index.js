//Import the firebase function sdk.
const functions = require('firebase-functions');

// Import and initialize the Firebase Admin SDK.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const users = 'users';
const posts = 'posts';
const replies = 'replies';
const subjects = 'subjects';

/**
* Cloud function called on creation of a new user in the database. 
*/
exports.onUserCreated 
	= functions.database.ref(`${users}/{userUid}`).onWrite(event => {
	
	//Prevent the cloud function from triggering on update and delete.
	if (event.data.previous.exists() || !event.data.exists())
		return;
});

exports.onPostCreated 
	= functions.database.ref(`${posts}/{postUid}`).onWrite(event => {
	
	//Prevent the cloud function from triggering on update and delete.
	if (event.data.previous.exists() || !event.data.exists())
		return;

	const post = event.data.val();
	const uid = event.params.postUid;
	const title = post.info.title;
	const timestamp = event.timestamp;
	const subjectUid= post.info.subjectUid;

	console.log(`onPostCreated uid = '${uid}' name = '${title}'`);

	
	return Promise.all([
		
		//Insert the created date and uid to the created post.
		event.data.adminRef.update({
			'created' : admin.database.ServerValue.TIMESTAMP,
			'uid' : uid
		}),

		//Increase the number of subjects.
		new Promise(function(resolve, reject) {
		event.data.adminRef.root
			.child(`${subjects}/${subjectUid}/numberOfPosts`).transaction(function(numberOfPosts) {
				console.log("Setting number of posts", numberOfPosts);
				return (numberOfPosts || 0) + 1;;
			}, () => resolve(null));
		})
	]);

	//return event.data.adminRef.update({
	//	'created' : admin.database.ServerValue.TIMESTAMP,
	//	'uid' : uid
	//});
});

exports.onReplyCreated 
	= functions.database.ref(`${replies}/{postUid}/{replyUid}`).onWrite(event => {
	
	//Prevent the cloud function from triggering on update and delete.
	if (event.data.previous.exists() || !event.data.exists())
		return;

	const reply = event.data.val();
	const uid = event.params.replyUid;
	const title = reply.info.title;
	const timestamp = event.timestamp;

	console.log(`onReplyCreated uid = '${uid}'`);

	return event.data.adminRef.update({
		'created' : admin.database.ServerValue.TIMESTAMP,
		'uid' : uid
	});
});
